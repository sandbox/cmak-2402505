;(function($){
    var count = 0;
    $(document).ready(function() {
        // Speed of the automatic slideshow
        var slideshowSpeed = 15000;

        // Variable to store the images we need to set as background
        // which also includes some text and url's.
        eval( 'var from_json_object = ' + Drupal.settings.full_bg_img_slider.images + ';' );
        var photos = from_json_object;

    	// Backwards navigation
    	$("#back").click(function() {
    		stopAnimation();
    		navigate("back");
    	});

    	// Forward navigation
    	$("#next").click(function() {
    		stopAnimation();
    		navigate("next");
    	});

    	var interval;
    	$("#control").toggle(function(){
    		stopAnimation();
    	}, function() {
    		// Show the next image
    		navigate("next");

    		// Start playing the animation
    		interval = setInterval(function() {
    			navigate("next");
    		}, slideshowSpeed);
    	});

    	var activeContainer = 0;
    	var currentImg = 0;
    	var animating = false;
    	var navigate = function(direction) {
    		// Check if no animation is running. If it is, prevent the action
    		if(animating) {
    			return;
    		}

    		// Check which current image we need to show
    		if(direction == "next") {
    			currentImg++;
    			if(currentImg == photos.length + 1) {
    				currentImg = 1;
    			}
    		} else {
    			currentImg--;
    			if(currentImg == 0) {
    				currentImg = photos.length;
    			}
    		}

    		// Check which container we need to use
    		var currentContainer = activeContainer;
    		if(activeContainer == 0) {
    			activeContainer = 1;
    		} else {
    			activeContainer = 0;
    		}
    		showImage(photos[currentImg - 1], currentContainer, activeContainer);
            count++;
    	};
    	var currentZindex = -1;
    	var showImage = function(photoObject, currentContainer, activeContainer) {
    		animating = true;

    		// Make sure the new container is always on the background
    		currentZindex--;

            // set the background image size so that it does not contain blank margins in the side
            var bgsize = jQuery('body').width()+'px auto';
    		// Set the background image of the new active container
    		$("#headerimg" + activeContainer).css({
    			"background-image" : "url(" + photoObject.image + ")",
    			"display" : "block",
    			"z-index" : currentZindex
                //"background-size" : bgsize
    		});

            $("#headerimg" + currentContainer).animate(500, function(){});

            // Hide the header text
            if(count != 0) {
                $("#headertxt").fadeOut(1000, function(){
                    // Set the new header text
                	$("#title").html(photoObject.title);
                	$("#imgurl").attr("href", photoObject.url).html(photoObject.urltext);
                    $("body").css("background-color", photoObject.bgColor);
                    if(photoObject.prodImage != '') {
                        $("#prodimage").attr("src", photoObject.prodImage);
                    }
                    else {
                        $("#prodimage").hide();
                    }
                });
            }
            else {
                $("#title").html(photoObject.title);
            	$("#imgurl").attr("href", photoObject.url).html(photoObject.urltext);
                $("body").css("background-color", photoObject.bgColor);
                if(photoObject.prodImage != '') {
                    $("#prodimage").attr("src", photoObject.prodImage);
                }
                else {
                    $("#prodimage").hide();
                }
            }
    		// Fade out the current container
    		// and display the header text when animation is complete
    		$("#headerimg" + currentContainer).fadeOut(2000, function() {
    			setTimeout(function() {
    				animating = false;
    			}, 500);
    		});
            $("#headertxt").fadeIn(1000, function(){});
    	};

    	var stopAnimation = function() {
    		// Change the background image to "play"
    		$("#control").css({ "background-image" : "url(images/btn_play.png)" });

    		// Clear the interval
    		clearInterval(interval);
    	};

    	// We should statically set the first image
    	navigate("next");

    	// Start playing the animation
    	interval = setInterval(function() {
    		navigate("next");
    	}, slideshowSpeed);

        //this is for pausing the jquery effect
    	jQuery('#block-block-10').mouseover(function(){
            stopAnimation();
        });
        jQuery('#block-block-10').mouseout(function(){
    		// Start playing the animation
    		interval = setInterval(function() {
    			navigate("next");
    		}, slideshowSpeed);
        });
    });
})(jQuery);